import math
mean = 2.5 #the avg. successes
x = 5 #whose probab we calculate

def fac(x): #factorial
  if x == 0:
    return 1
  else:
    return x*fac(x-1)  
  

probab = math.pow(mean, x)*math.exp(-mean)/fac(x)
print(probab)
