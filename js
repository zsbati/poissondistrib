let mean = 2.5; //the average
let x = 5; //the value whose probab we want


function fac(x){
  if (x == 0){
    return 1;
  } else {
    return x*fac(x-1);
  }
}

let probab = Math.pow(mean, x)*Math.exp(-mean)/fac(x);

console.log(probab);
